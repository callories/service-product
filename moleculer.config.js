const config = require('config')

const brokerConfig = {
  transporter: {
    type: 'NATS',
    options: {
      url: config.get('transporter.nats.uri'),
      user: config.get('transporter.nats.user'),
      pass: config.get('transporter.nats.pass'),
    },
  },
  namespace: config.get('namespace'),
  metrics: true,
}

module.exports = brokerConfig
