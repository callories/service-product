const DbService = require('moleculer-db')
const MongoDBAdapter = require('moleculer-db-adapter-mongo')
const pkg = require('./../package.json')

const ProductService = {
  name: 'product-category',

  mixins: [DbService],
  adapter: new MongoDBAdapter('mongodb://calories-mongo/calories'),
  collection: 'product-categories',

  actions: {
    version: () => pkg.version,
  },
  methods: {},
  hooks: {},
}

module.exports = ProductService
