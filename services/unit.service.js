const DbService = require('moleculer-db')
const MongoDBAdapter = require('moleculer-db-adapter-mongo')

module.exports = {
  name: 'unit',
  mixins: [DbService],
  adapter: new MongoDBAdapter('mongodb://calories-mongo/calories'),
  collection: 'units',

}
