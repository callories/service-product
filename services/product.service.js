const DbService = require('moleculer-db')
const MongoDBAdapter = require('moleculer-db-adapter-mongo')
const pkg = require('./../package.json')

const ProductService = {
  name: 'product',

  mixins: [DbService],
  adapter: new MongoDBAdapter('mongodb://calories-mongo/calories'),
  collection: 'products',
  settings: {
    populate: {
      category: 'product-category.get',
    },
  },
  actions: {
    version: () => pkg.version,
    'rating.update': {
      params: {
        id: 'string',
        rating: 'number',
      },
      async handler (ctx) {
        try {
          const { id, rating } = ctx.params
          const product = await this.actions.get({
            id,
            populate: ctx.params.populate || [],
          })
          const { votes: oldVotes, rating: oldRating } = product
          const med = oldVotes * oldRating
          const newVotes = (oldVotes || 0) + 1
          const newRating = ((med || 0) + (rating || 0)) / (newVotes)
          return this.actions.update({
            id,
            votes: newVotes,
            rating: newRating,
          })
        } catch (e) {
          this.logger.error(e)
          return Promise.reject(e)
        }
      },
    },
  },
  methods: {},
  hooks: {},
}

module.exports = ProductService
